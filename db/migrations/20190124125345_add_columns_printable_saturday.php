<?php

use Phinx\Migration\AbstractMigration;


class AddColumnsPrintableSaturday extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('wame_dhl');
        $table->addColumn('exported_user_id', 'integer', ['null' => true, 'after' => 'exported_at'])
                ->addColumn('printed_at', 'datetime', ['null' => true, 'after' => 'exported_user_id'])
                ->addColumn('printed_user_id', 'integer', ['null' => true, 'after' => 'printed_at'])
                ->addColumn('saturday', 'boolean', ['after' => 'printed_user_id'])
                ->addForeignKey('exported_user_id', 'wame_user', 'id', ['delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'])
                ->addForeignKey('printed_user_id', 'wame_user', 'id', ['delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'])
                ->update();
    }

}
