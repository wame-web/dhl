<?php

use Phinx\Migration\AbstractMigration;

class CreateDhlTable extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('wame_dhl');
        $table->addColumn('order_id', 'integer')
            ->addColumn('package_number', 'biginteger', ['limit' => 15, 'null' => true])
            ->addColumn('package_count', 'integer', ['default' => 1])
            ->addColumn('exported_at', 'datetime', ['null' => true])
            ->addColumn('status', 'integer', ['limit' => 3, 'null' => true])
            ->addIndex(['package_number'], ['unique' => true])
            ->addIndex(['order_id', 'package_count'], ['unique' => true])
            ->addForeignKey('order_id', 'wame_shop_order', 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->create();
    }


    public function down()
    {
        $this->table('wame_dhl')->drop()->save();
    }

}
