<?php

use Phinx\Migration\AbstractMigration;


class AddDhlPrintedProtocolColumn extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('wame_dhl');
        $table->addColumn('printed_protocol_at', 'datetime', ['null' => true, 'after' => 'printed_user_id'])
            ->addColumn('printed_protocol_user_id', 'integer', ['null' => true, 'after' => 'printed_protocol_at'])
            ->addForeignKey('printed_protocol_user_id', 'wame_user', 'id', ['delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'])
            ->update();
    }

}
