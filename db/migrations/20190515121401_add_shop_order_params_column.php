<?php

use Phinx\Migration\AbstractMigration;


class AddShopOrderParamsColumn extends AbstractMigration
{
    /** {@inheritdoc} */
    public function up()
    {
        $table = $this->table('wame_shop_order');
        $table->addColumn('params', 'string')
                ->save();
    }


    /** {@inheritdoc} */
    public function down()
    {
        $table = $this->table('wame_shop_order');
        $table->removeColumn('params')
                ->save();
    }

}
