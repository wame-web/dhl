<?php

use Phinx\Migration\AbstractMigration;


class CreateDhlProductTable extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('wame_dhl_product');
        $table->addColumn('code', 'integer', ['limit' => 3])
            ->addColumn('name', 'string')
            ->addColumn('cod', 'integer', ['limit' => 1])
            ->addColumn('min', 'biginteger', ['limit' => 15])
            ->addColumn('max', 'biginteger', ['limit' => 15])
            ->addColumn('next', 'biginteger', ['limit' => 15])
            ->create();

        $data = [
            ['code' => 101, 'name' => 'DHL PARCEL Slovensko', 'cod' => 0, 'min' => 0, 'max' => 0, 'next' => 0],
            ['code' => 102, 'name' => 'DHL PARCEL Slovensko - dobierka', 'cod' => 1, 'min' => 0, 'max' => 0, 'next' => 0],
            ['code' => 103, 'name' => 'DHL PARCEL International', 'cod' => 0, 'min' => 0, 'max' => 0, 'next' => 0],
            ['code' => 104, 'name' => 'DHL PARCEL International - dobierka', 'cod' => 1, 'min' => 0, 'max' => 0, 'next' => 0],
            ['code' => 109, 'name' => 'DHL Parcel Import', 'cod' => 0, 'min' => 0, 'max' => 0, 'next' => 0],
            ['code' => 112, 'name' => 'DHL ParcelConnect', 'cod' => 0, 'min' => 0, 'max' => 0, 'next' => 0],
            ['code' => 113, 'name' => 'DHL ParcelConnect - dobierka', 'cod' => 1, 'min' => 0, 'max' => 0, 'next' => 0]
        ];

        $this->insert('wame_dhl_product', $data);
    }


    public function down()
    {
        $this->table('wame_dhl_product')->drop()->save();
    }

}
