<?php

use Phinx\Migration\AbstractMigration;


class AddSiteAppsDhl extends AbstractMigration
{
    public function change()
    {
        $data = [
            'app_id'    =>'dhl',
            'version'  => 1,
            'title'  => 'DHL',
            'description'  => '',
            'default_module'  => 'Admin',
            'default_presenter'  => 'Dhl',
            'default_action'  => 'default',
            'default_id'  => null,
            'element_class'  => '',
            'image_icon'  => '',
            'parent_id'  => 4,
            'in_menu'  => 1,
            'sort'  => 8,
            'status'  => 1
        ];

        $table = $this->table('wame_site_apps');
        $table->insert($data);
        $table->saveData();
    }

}
