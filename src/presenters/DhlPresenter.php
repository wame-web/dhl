<?php

namespace App\Presenters;

use WameCms\Dhl\Model\PackageTracking;


class DhlPresenter extends BasePresenter
{
    /** @var PackageTracking @inject */
    public $packageTracking;


    public function actionPackageTracking()
    {
        $this->packageTracking->run();

        exit;
    }

}
