<?php

namespace WameCms\Dhl\Controls;

use App\Model\Shop\ShopOrderUserInfoRepository;
use Nette\Application\AbortException;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;
use WameCms\Dhl\Model\Dhl;
use WameCms\Dhl\Model\Labels;
use WameCms\Dhl\Model\PickupOrders;
use WameCms\Dhl\Model\SendPackages;
use WameCms\Dhl\Model\TransferList;
use WameCms\Dhl\Repositories\DhlRepository;


trait DhlExportControlTrait
{
    /** @var DhlExportControlFactory @inject */
    public $dhlExportControlFactory;

    protected function createComponentDhlExport()
    {
        return $this->dhlExportControlFactory->create();
    }
}


interface DhlExportControlFactory
{
    /** @return DhlExportControl */
    public function create();
}


class DhlExportControl extends Control
{
    /** @var Dhl */
    private $dhl;

    /** @var Labels */
    private $labels;

    /** @var PickupOrders */
    private $pickupOrders;

    /** @var SendPackages */
    private $sendPackages;

    /** @var TransferList */
    private $transferList;

    /** @var DhlRepository */
    private $dhlRepository;

    /** @var ShopOrderUserInfoRepository */
    private $shopOrderUserInfoRepository;


    public function __construct(
        Dhl $dhl,
        Labels $labels,
        PickupOrders $pickupOrders,
        SendPackages $sendPackages,
        TransferList $transferList,
        DhlRepository $dhlRepository,
        ShopOrderUserInfoRepository $shopOrderUserInfoRepository
    ) {
        parent::__construct();

        $this->dhl = $dhl;
        $this->labels = $labels;
        $this->pickupOrders = $pickupOrders;
        $this->sendPackages = $sendPackages;
        $this->transferList = $transferList;
        $this->dhlRepository = $dhlRepository;
        $this->shopOrderUserInfoRepository = $shopOrderUserInfoRepository;
    }


    protected function createComponentForm($name)
    {
        $form = new Form();

        $form->setMethod(Form::GET);

//        $form->addText('from', _('Od'))
//                ->setType('date');
//
//        if ($this->getPresenter()->getParameter('from')) {
//            $form['from']->setDefaultValue($this->getPresenter()->getParameter('from'));
//        } else {
//            $last = $this->dhlRepository->getTable()->select('exported_at')->order('exported_at DESC')->limit(1)->fetch();
//
//            if ($last) $form['from']->setDefaultValue(date('Y-m-d', strtotime($last['exported_at'])));
//        }
//
//        $form->addText('to', _('Do'))
//                ->setType('date')
//                ->setDefaultValue($this->getPresenter()->getParameter('from'));

        $form->addSubmit('generate_labels', _('Vytlačiť etikety'));
        $form->addSubmit('generate_transfer_list', _('Vytlačiť odovzdávací protokol'));
        $form->addSubmit('closing_day', _('Odoslať balíčky do DHL'));
        $form->addSubmit('create_pickup', _('Zavolať kuriéra pre balíčky'));
        $form->addSubmit('saturday', _('Zmeniť sobotné doručenie'));

        $form->onSuccess[] = [$this, 'formSuccess'];

        return $form;
    }


    public function formSuccess(Form $form, $values)
    {
        try {
//            $from = $values['from'];
//            $to = $values['to'];
            $ids = $form->getHttpData()['ids'];
            if (is_string($ids)) $ids = explode(',', $ids);

            $shopSettings = $this->getPresenter()->shopSettings;

            if ($form['generate_labels']->isSubmittedBy()) {
                $this->labels->generate($shopSettings, $ids /*$form, $to*/);
            } elseif ($form['generate_transfer_list']->isSubmittedBy()) {
                $this->transferList->generate($shopSettings, $ids /*$form, $to*/);
            } elseif ($form['closing_day']->isSubmittedBy()) {
                $this->sendPackages->createPackages($shopSettings, $ids /*$form, $to*/);
                $this->getPresenter()->flashMessage(_('Balíčky boli úspešne odoslané do DHL'), 'success');
            } elseif ($form['create_pickup']->isSubmittedBy()) {
                $this->pickupOrders->createPickupOrders($shopSettings, $ids /*$form, $to*/);
                $this->getPresenter()->flashMessage(_('Žiadosť o kuriéra bola úspešne odoslaná do DHL'), 'success');
            } elseif ($form['saturday']->isSubmittedBy()) {
                $this->dhlRepository->changeSaturday($ids);
                $this->getPresenter()->flashMessage(_('Sobotné doručenie bolo zmenene na zvolenych balíčkoch'), 'success');
            }

            $this->getPresenter()->redirect('this');
        } catch (\Exception $e) {
            if ($e instanceof AbortException) throw $e;

            $form->addError($e->getMessage());
        }
    }


    public function render($template = 'default.latte')
    {
        $options = $this->dhl->getOptions();
        $list = $this->dhlRepository->getObjectPairs(['order.delivery_method_id' => $options['deliveryMethod'], 'order.params LIKE ?' => '%expedovat: 1%'], 'order_id', 'order.complete_date DESC');
        $userInfoList = $this->shopOrderUserInfoRepository->getObjectPairs(['shop_order_id' => array_keys($list)], 'shop_order_id');

        $this->template->options = $this->dhl->getOptions();
        $this->template->list = $list;
        $this->template->userInfoList = $userInfoList;

        $this->template->setFile(__DIR__ . '/' . $template);
        $this->template->render();
    }

}
