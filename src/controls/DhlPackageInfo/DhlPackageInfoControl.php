<?php

namespace WameCms\Dhl\Controls;

use Nette\Application\UI\Control;
use Nette\Database\Table\ActiveRow;
use WameCms\Dhl\Repositories\DhlRepository;


interface DhlPackageInfoControlFactory
{
    /** @return DhlPackageInfoControl */
    public function create();
}


class DhlPackageInfoControl extends Control
{
    /** @var ActiveRow */
    private $order;

    /** @var string */
    private $templateName = 'default.latte';

    /** @var DhlRepository */
    private $dhlRepository;


    public function __construct(
        DhlRepository $dhlRepository
    ) {
        parent::__construct();

        $this->dhlRepository = $dhlRepository;
    }


    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }


    public function setTemplateName($templateName)
    {
        $this->templateName = $templateName;

        return $this;
    }


    public function render()
    {
        $this->template->packages = $this->dhlRepository->getOrderPackages($this->order['id']);
        $this->template->statusList = \WameCms\Dhl\Repositories\DhlRepository::getStatusList();

        $this->template->setFile(__DIR__ . '/' . $this->templateName);
        $this->template->render();
    }

}
