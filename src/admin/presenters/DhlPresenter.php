<?php

namespace App\AdminModule\Presenters;

use App\Model\Shop\ShopSettingsRepository;
use WameCms\Dhl\Admin\Grids\DhlGridFactory;
use WameCms\Dhl\Admin\Forms\FilterFormFactory;
use WameCms\Dhl\Controls\DhlExportControlFactory;


class DhlPresenter extends AdminPresenter
{
    /** @var ShopSettingsRepository @inject */
    public $shopSettingsRepository;

    /** @var FilterFormFactory @inject */
    public $form;

    /** @var DhlExportControlFactory @inject */
    public $dhlExport;

    /** @var DhlGridFactory @inject */
    public $grid;

    /** @var array */
    public $shopSettings;

    /** @var array */
    private $filter;


    public function actionDefault()
    {
        $this->shopSettings = $this->shopSettingsRepository->getAllSettings();
        $this->filter = $this->getParameter('filter');

        $this->addComponent($this->form->create($this->filter), 'filter');
        $this->addComponent($this->dhlExport->create(), 'dhlExport');
        $this->addComponent($this->grid->create($this->filter), 'grid');
    }


    public function renderDefault()
    {
        $this->template->siteTitle = _('DHL');
        $this->template->filter = $this->filter;
    }

}
