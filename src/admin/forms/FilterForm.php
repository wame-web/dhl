<?php

namespace WameCms\Dhl\Admin\Forms;

use App\Model\Shop\CurrencyRepository;
use App\Model\System\CountryRepository;
use Nette\Application\UI\Form;
use WameCms\Dhl\Repositories\DhlProductRepository;


interface FilterFormFactory
{
    /** @return FilterForm */
    public function create($filter);
}


class FilterForm extends Form
{
    /** @var array */
    private $filter;

    /** @var CountryRepository */
    private $countryRepository;

    /** @var CurrencyRepository */
    private $currencyRepository;


    public function __construct(
        $filter,
        CountryRepository $countryRepository,
        CurrencyRepository $currencyRepository
    ) {
        $this->filter = $filter;
        $this->countryRepository = $countryRepository;
        $this->currencyRepository = $currencyRepository;

        $this->build();
    }


    protected function build()
    {
        $this->addText('order_id', _('Číslo objednávky'));

        $this->addText('package_number', _('Číslo zásielky'));

        $this->addText('company_name', _('Názov firmy'));

        $this->addText('name', _('Kontaktná osoba'));

        $this->addText('city', _('Mesto'));

        $this->addText('zip_code', _('PSČ'));

        $this->addText('cod_from', _('Dobierka od'));

        $this->addText('cod_to', _('Dobierka do'));

        $this->addSelect('currency', _('Mena'), $this->currencyRepository->getPairs([], 'id', 'code', 'code ASC'))
                ->setPrompt('-' . _('nezadané') . '-');

        $this->addText('invoice_number', _('Var. symbol'));

        $this->addSelect('printed', _('Vytlačená'), [0 => _('Nie'), 1 => _('Áno')])
                ->setPrompt('-' . _('nezadané') . '-');

        $this->addSelect('exported', _('Odoslané'), [0 => _('Nie'), 1 => _('Áno')])
                ->setPrompt('-' . _('nezadané') . '-');

        $this->addText('date_from', _('Vytvorené od'))
                ->setType('date');

        $this->addText('date_to', _('Vytvorené do'))
                ->setType('date');

        $this->addSelect('type', _('Typ zásielky'), DhlProductRepository::getTypeList())
                ->setPrompt('-' . _('nezadané') . '-');

        $this->addSelect('country', _('Krajina'), $this->countryRepository->getPairs([], 'id', 'title', 'title ASC'))
                ->setPrompt('-' . _('nezadané') . '-');

        $this->addSubmit('submit', _('Zobraz'));

        if ($this->filter) {
            $this->setDefaults($this->filter);
        }

        $this->onSuccess[] = [$this, 'onSuccess'];
    }


    public function onSuccess(Form $form, $values)
    {
        $filter = [];

        foreach ($values as $key => $value) {
            if (!$value) continue;
            if ($key == 'submit') continue;

            $filter[$key] = $value;
        }

        $form->getPresenter()->redirect('this', ['filter' => $filter]);
    }

}
