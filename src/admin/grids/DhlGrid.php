<?php

namespace WameCms\Dhl\Admin\Grids;

use App\Model\Shop\ShopOrderRepository;
use App\Model\Shop\ShopOrderUserInfoRepository;
use App\Model\Shop\ShopSettingsRepository;
use Grido\Grid;
use Nette\Application\LinkGenerator;
use Nette\Application\UI\Control;
use Nette\Utils\Html;
use WameCms\Dhl\Model\Dhl;
use WameCms\Dhl\Model\Labels;
use WameCms\Dhl\Model\Packages;
use WameCms\Dhl\Repositories\DhlProductRepository;
use WameCms\Dhl\Repositories\DhlRepository;


interface DhlGridFactory
{
    /** @return DhlGrid */
    public function create($filter);
}


class DhlGrid extends Control
{
    /** @var array */
    private $filter;

   /** @var Dhl */
    private $dhl;

    /** @var Labels */
    private $labels;

    /** @var Packages */
    private $packages;

    /** @var DhlRepository */
    private $repository;

    /** @var ShopOrderRepository */
    private $shopOrderRepository;

    /** @var ShopOrderUserInfoRepository */
    private $shopOrderUserInfoRepository;

    /** @var ShopSettingsRepository */
    private $shopSettingsRepository;

    /** @var LinkGenerator */
    private $linkGenerator;

    /** @var array */
    private $options;

    /** @var array */
    private $userInfoList;


    public function __construct(
        $filter,
        Dhl $dhl,
        Labels $labels,
        Packages $packages,
        DhlRepository $dhlRepository,
        ShopOrderRepository $shopOrderRepository,
        ShopOrderUserInfoRepository $shopOrderUserInfoRepository,
        ShopSettingsRepository $shopSettingsRepository,
        LinkGenerator $linkGenerator
    ) {
        $this->filter = $filter;
        $this->dhl = $dhl;
        $this->labels = $labels;
        $this->packages = $packages;
        $this->repository = $dhlRepository;
        $this->shopOrderRepository = $shopOrderRepository;
        $this->shopOrderUserInfoRepository = $shopOrderUserInfoRepository;
        $this->shopSettingsRepository = $shopSettingsRepository;
        $this->linkGenerator = $linkGenerator;

        $this->options = $dhl->getOptions();
    }


    public function render()
    {
        $this->template->setFile(ABSOLUTE_PATH . '/../app/adminModule/components/grids/Grid.latte');
        $this->template->render();
    }

    
    protected function createComponentGrid($name)
    {
		$grid = new Grid($this, $name);
		$presenter = $this->getPresenter();

        $options = $this->dhl->getOptions();

        if ($this->filter) {
            $list = $this->getListByFilter($options);
        } else {
            $list = $this->repository->findBy(['(order.delivery_method_id = ? AND order.params LIKE ?) OR order.exported_at IS NOT NULL' => [$options['deliveryMethod'], '%expedovat: 1%'], 'order.order_id != ?' => ''], 'order.complete_date DESC')->fetchAll();
        }

        $this->userInfoList = $this->shopOrderUserInfoRepository->getObjectPairs(['shop_order_id' => array_column($list, 'order_id')], 'shop_order_id');

//        $list = $this->repository->findBy(['id' => array_keys($this->userInfoList)]);
       
		$grid->setModel($list);
		$grid->setDefaultSort(['id' => 'DESC']);
		$grid->setFilterRenderType('inner');
		$grid->setDefaultPerPage(100);
		$grid->setRememberState(true);
		$grid->setTemplateFile('../app/adminModule/components/grids/admin.latte');
		$grid->tablePrototype->class = 'table table-hover borderless';

		$this->getColumns($grid);

        $grid->addActionHref('view', _('Detaily'), 'ShopOrders:detail')
                ->setCustomRender(function ($item) {
                    return Html::el('a')->href($this->linkGenerator->link('Admin:ShopOrders:detail', ['id' => $item->order_id]))->addAttributes(['class' => 'btn btn-default btn-xs pull-right'])->setText(_('Detaily'));
                });

//        $grid->setOperation([], function ($operation, $ids) { });
		
		return $grid;
	}


	private function getColumns(Grid &$grid)
    {
        $self = $this;

        $grid->addColumnText('order_id', _('Číslo objednávky'))
                ->setColumn('order.order_id')
                ->setCustomRender(function ($item) use ($self) {
                    $order = $item->order;

                    $return = '';

                    if ($item->exported_at == null || ($item->exported_at != null && ($item->printed_at == null || $item->printed_protocol_at == null))) {
                        $return = Html::el('label')->setHtml(Html::el('input')->type('checkbox')->value($item->id) . ' ' . $order->order_id);
                    } else {
                        $return = Html::el('div')->setStyle('margin-left: 15px;')->setHtml($order->order_id);
                    }

                    if ($order->count_packages > 1) {
                        $return .= Html::el('small')->setText(_('Balík') . ': ' . $item->package_count . '/' . $order->count_packages);
                    }

                    return $return;
                })
                ->getCellPrototype()->addAttributes(['class' => 'bg-primary']);

        $grid->addColumnText('package_number', _('Číslo zásielky'));

        $grid->addColumnText('complete_date', _('Vytvorené'))
                ->setColumn('order.complete_date')
                ->setCustomRender(function ($item) {
                    $order = $item->order;

                    $return = Html::el('div')->setText(date('d.m.Y', strtotime($order->complete_date)));
                    $return .= Html::el('div')->setText(date('H:i:s', strtotime($order->complete_date)));

                    return $return;
                });

        $grid->addColumnText('user_info', _('Adresa príjemcu'))
                ->setCustomRender(function ($item) use ($self) {
                    if (!isset($self->userInfoList[$item->order_id])) return '-';

                    $userInfo = $self->userInfoList[$item->order_id];

                    if ($userInfo->setOtherAddress == 1) {
                        $return = Html::el('div')->setStyle(['white-space' => 'nowrap'])->setText($userInfo->shipping_realFirstName . ' ' . $userInfo->shipping_realLastName);
                        $return .= Html::el('div')->setStyle(['white-space' => 'nowrap'])->setText($userInfo->shipping_zipCode . ' ' . $userInfo->shipping_city . ', ' . ($userInfo->shipping_country_id ? $userInfo->shipping_country->title : $userInfo->shipping_state));
                    } else {
                        if ($userInfo->setCompanyInfo == 1) {
                            $return = Html::el('div')->setStyle(['white-space' => 'nowrap'])->setText($userInfo->companyName . ' - ' . $userInfo->realFirstName . ' ' . $userInfo->realLastName);
                            $return .= Html::el('div')->setStyle(['white-space' => 'nowrap'])->setText($userInfo->companyZipCode . ' ' . $userInfo->companyCity . ', ' . ($userInfo->companyCountry_id ? $userInfo->companyCountry->title : $userInfo->companyState));
                        } else {
                            $return = Html::el('div')->setStyle(['white-space' => 'nowrap'])->setText($userInfo->realFirstName . ' ' . $userInfo->realLastName);
                            $return .= Html::el('div')->setStyle(['white-space' => 'nowrap'])->setText($userInfo->zipCode . ' ' . $userInfo->city . ', ' . ($userInfo->country_id ? $userInfo->country->title : $userInfo->state));
                        }
                    }

                    if ($userInfo->setOtherAddress == 1) {
                        $return .= Html::el('div')->setStyle(['white-space' => 'nowrap'])->setText(_('Tel.:') . ' ' . $userInfo->shipping_phone ? $userInfo->shipping_phone : $userInfo->phone);
                    } else {
                        $return .= Html::el('div')->setStyle(['white-space' => 'nowrap'])->setText(_('Tel.:') . ' ' . $userInfo->phone);
                    }

                    return $return;
                });

        $grid->addColumnText('type', _('Typ zásielky'))
                ->setCustomRender(function ($item) use ($self) {
                    if (!isset($self->userInfoList[$item->order_id])) return '-';

                    $return = $self->packages->setOptions($self->options)->getPackProductType($item->order, $self->userInfoList[$item->order_id]);

                    return Html::el('small')->setText(DhlProductRepository::getType($return));
                });

        $grid->addColumnText('cod', _('Dobierka'))
                ->setCustomRender(function ($item) use ($self) {
                    $order = $item->order;

                    if ($order->cod > 0) {
                        $return = $order->cod;
                    } elseif ($order->cod == 0 && $order->payment_method_id == $self->options['paymentMethodCOD']) {
                        $return = $order->order_price_with_tax + $order->delivery_price + $order->payment_price;
                    } else {
                        return _('NIE');
                    }

                    return Html::el('strong')->setText(number_format($return * $order->currency_coefficient, 2, ',', ' '));
                });

        $grid->addColumnText('currency', _('Mena'))
                ->setColumn('order.currency.code');

        $grid->addColumnText('invoice_number', _('Var. symbol'))
                ->setColumn('order.invoice_number');

        $grid->addColumnText('saturday', _('Sobotné doruč.'))
            ->setCustomRender(function ($item) {
                return $item->saturday == 1 ? Html::el('strong')->setText(_('ÁNO')) : _('NIE');
            });

        $grid->addColumnText('exported_at', _('Odos. do DHL'))
                ->setCustomRender(function ($item) {
                    if (!$item->exported_at) return _('NIE');

                    $return = Html::el('div')->addClass('exported')->setText(date('d.m.Y', strtotime($item->exported_at)));
                    $return .= Html::el('div')->setText(date('H:i:s', strtotime($item->exported_at)));

                    return $return;
                });

        $grid->addColumnText('printed_at', _('Vytlačená etiketa'))
            ->setCustomRender(function ($item) {
                if (!$item->printed_at) return _('NIE');

                $return = Html::el('div')->addClass('printed')->setText(date('d.m.Y', strtotime($item->printed_at)));
                $return .= Html::el('div')->setText(date('H:i:s', strtotime($item->printed_at)));

                return $return;
            });

        $grid->addColumnText('printed_protocol_at', _('Vytlačený protokol'))
                ->setCustomRender(function ($item) {
                    if (!$item->printed_protocol_at) return _('NIE');

                    $return = Html::el('div')->addClass('printed_protocol')->setText(date('d.m.Y', strtotime($item->printed_protocol_at)));
                    $return .= Html::el('div')->setText(date('H:i:s', strtotime($item->printed_protocol_at)));

                    return $return;
                });

        $grid->addColumnText('note', _('Poznámka'))
                ->setColumn('order.internal_note')
                ->setCustomRender(function ($item) {
                    return Html::el('small')->setText($item->order->internal_note);
                });
    }

    private function getListByFilter($options)
    {
        $query = "SELECT d.id, d.order_id "
            . "FROM wame_dhl AS d "
            . "LEFT JOIN wame_shop_order AS o ON o.id = d.order_id "
            . "LEFT JOIN wame_shop_order_user_info AS u ON u.shop_order_id = o.id "
            . "WHERE o.delivery_method_id = ? AND o.order_id != ? ";

//        $args = [$options['deliveryMethod'], $options['orderStatus']];
        $args = [$options['deliveryMethod'], ''];

        foreach ($this->filter as $key => $value) {
            if (!$value) continue;

            $count = 1;

            switch ($key) {
                case 'order_id':
                    $query .= "AND o.order_id = ? ";
                    break;
                case 'package_number':
                    $query .= "AND d.package_number = ? ";
                    break;
                case 'company_name':
                    $query .= "AND u.companyName = ? ";
                    break;
                case 'name':
                    $where = "";

                    foreach (explode(' ', $value) as $val) {
                        $where .= "o.shopper_full_name LIKE ? OR u.realFirstName LIKE ? OR u.realLastName LIKE ? OR u.shipping_realFirstName LIKE ? OR u.shipping_realLastName LIKE ? OR ";

                        for ($i = 1; $i <= 5; $i++) {
                            $args[] = '%' . $val . '%';
                        }
                    }

                    $query .= "AND (" . substr($where, 0, -4) . ") ";
                    $count = 0;
                    break;
                case 'city':
                    $query .= "AND (u.city = ? OR u.companyCity = ? OR u.shipping_city = ?) ";
                    $count = 3;
                    break;
                case 'zip_code':
                    $query .= "AND (u.zipCode = ? OR u.companyZipCode = ? OR u.shipping_zipCode = ?) ";
                    $count = 3;
                    break;
                case 'currency':
                    $query .= "AND o.currency_id = ? ";
                    break;
                case 'invoice_number':
                    $query .= "AND o.invoice_number LIKE ? ";
                    $value = '%' . $value . '%';
                    break;
                case 'printed':
                    if ($value == 0) {
                        $query .= "AND d.printed_at IS NULL ";
                    } else {
                        $query .= "AND d.printed_at IS NOT NULL ";
                    }

                    $count = 0;
                    break;
                case 'exported':
                    if ($value == 0) {
                        $query .= "AND d.exported_at IS NULL ";
                    } else {
                        $query .= "AND d.exported_at IS NOT NULL ";
                    }

                    $count = 0;
                    break;
                case 'date_from':
                    $query .= "AND DATE(o.complete_date) >= ? ";
                    $value = date('Y-m-d', strtotime($value));
                    break;
                case 'date_to':
                    $query .= "AND DATE(o.complete_date) <= ? ";
                    $value = date('Y-m-d', strtotime($value));
                    break;
                case 'country':
                    $query .= "AND (u.country_id = ? OR u.companyCountry_id = ? OR u.shipping_country_id = ?) ";
                    $count = 3;
                    break;
                default:
                    $count = 0;
            }

            if ($count > 0) {
                for ($i = 1; $i <= $count; $i++) {
                    $args[] = $value;
                }
            }
        }

        $ids = $this->repository->db->queryArgs($query, $args)->fetchPairs('id', 'order_id');

        // Typ zasielky
        if (isset($this->filter['type'])) {
            $orders = $this->shopOrderRepository->getObjectPairs(['id IN (?)' => array_values($ids)], 'id');
            $userInfo = $this->shopOrderUserInfoRepository->getObjectPairs(['shop_order_id IN (?)' => array_values($ids)], 'shop_order_id');

            $oldIds = array_flip($ids);
            $ids = [];

            foreach ($orders as $orderId => $order) {
                $check = $this->packages->setOptions($options)->getPackProductType($order, $userInfo[$orderId]);

                if ($check == $this->filter['type']) {
                    $ids[$oldIds[$orderId]] = $orderId;
                }
            }
        }

        // Dobierka
        if (isset($this->filter['cod_from']) || isset($this->filter['cod_to'])) {
            $orders = $this->shopOrderRepository->getObjectPairs(['id IN (?)' => array_values($ids)], 'id');

            $oldIds = array_flip($ids);
            $ids = [];

            foreach ($orders as $orderId => $order) {
                if ($order->cod == 0) {
                    $price = $order->order_price_with_tax + $order->delivery_price + $order->payment_price;
                } else {
                    $price = $order->cod;
                }

                if ($order->payment_method_id != $options['paymentMethodCOD']) continue;
                if (isset($this->filter['cod_from']) && $price < $this->filter['cod_from']) continue;
                if (isset($this->filter['cod_to']) && $price > $this->filter['cod_to']) continue;

                $ids[$oldIds[$orderId]] = $orderId;
            }
        }

//        return $list = $this->repository->findBy(['wame_dhl.id IN (?)' => array_keys($ids), 'order.delivery_method_id' => $options['deliveryMethod'], 'order.status' => $options['orderStatus']], 'order.complete_date DESC')->fetchAll();
        return $list = $this->repository->findBy(['wame_dhl.id IN (?)' => array_keys($ids), 'order.delivery_method_id' => $options['deliveryMethod'], 'order.order_id != ?' => ''], 'order.complete_date DESC')->fetchAll();
    }

}