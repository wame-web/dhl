<?php

namespace WameCms\Dhl\DI;

use App\Components\ShoppingMethod\DeliveryMethodRegister;
use Nette;
use Nette\DI\CompilerExtension;
use WameCms\Dhl\Controls\DhlDeliveryMethod;
use WameCms\Dhl\Controls\DhlDeliveryMethodFactory;
use WameCms\Dhl\Events\ImportUpdateOrderStatus;
use WameCms\Dhl\Model\Dhl;
use WameCms\Dhl\Model\PackageNumber;
use WameCms\Dhl\Model\Packages;


class DhlExtension extends CompilerExtension
{
    private $defaults = [
        'customerId' => null,
        'username' => null,
        'password' => null,
        'depo' => null,
        'deliveryMethod' => null,
        'paymentMethodCOD' => null,
        'orderStatus' => null,
        'range' => 100,
        'pickupButton' => true
    ];


    public function loadConfiguration()
    {
        $builder = $this->getContainerBuilder();

        $this->config = $this->validateConfig($this->defaults);

        $this->compiler->loadDefinitions($builder, $this->loadFromFile(__DIR__ . '/../config/config.neon')['services'], $this->name);
    }


    public function beforeCompile()
    {
        parent::beforeCompile();

        $builder = $this->getContainerBuilder();

        $builder->getDefinition($builder->getByType(ImportUpdateOrderStatus::class))->addSetup('setOptions', [$this->config]);

        $builder->getDefinition($builder->getByType(Dhl::class))->addSetup('setOptions', [$this->config]);

        $builder->getDefinition($builder->getByType(Packages::class))->addSetup('setOptions', [$this->config]);

        $builder->getDefinition($builder->getByType(DeliveryMethodRegister::class))->addSetup('add', [$builder->getDefinition($builder->getByType(DhlDeliveryMethodFactory::class)), 'dhl']);
    }

}
