<?php

namespace WameCms\Dhl\Repositories;

use App\Model\BaseRepository;
use DhlMyApi\Package;


class DhlProductRepository extends BaseRepository
{
    public $tableName = 'dhl_product';


    /**
     * Get DHL product type list
     *
     * @return array
     */
    public static function getTypeList()
    {
        return [
            Package::TYPE_SK => 'DHL Parcel Slovensko',
            Package::TYPE_SK_COD => 'DHL Parcel Slovensko - dobierka',
            Package::TYPE_INTERNATIONAL => 'DHL Parcel International',
            Package::TYPE_INTERNATIOANL_COD => 'DHL Parcel International - dobierka',
            Package::TYPE_FORYOU_SK => 'DHL Parcel For You Slovensko',
            Package::TYPE_FORYOU_SK_COD => 'DHL Parcel For You Slovensko - dobierka',
            Package::TYPE_FORYOU_INTERNATIONAL => 'DHL Parcel For You International',
            Package::TYPE_FORYOU_INTERNATIONAL_COD => 'DHL Parcel For You International - dobierka',
            Package::TYPE_IMPORT => 'DHL Parcel Import',
            Package::TYPE_IMPORT_COD => 'DHL Parcel Import - dobierka'
        ];
    }


    /**
     * Get DHL product type
     *
     * @param int $type
     *
     * @return string|null
     */
    public static function getType($type)
    {
        if (isset(self::getTypeList()[$type])) {
            return self::getTypeList()[$type];
        }

        return null;
    }

}
