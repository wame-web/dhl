<?php

namespace WameCms\Dhl\Repositories;

use App\Model\BaseRepository;
use Nette\Database\SqlLiteral;


class DhlRepository extends BaseRepository
{
    public $tableName = 'dhl';


    /**
     * Get orders
     *
     * @return array
     */
    public function getOrders($ids /*$from = null, $to = null*/)
    {
        $args = [];

        $query = "SELECT d.* FROM " . $this->prefix . "dhl AS d "
            . "LEFT JOIN " . $this->prefix . "shop_order AS o ON o.id = d.order_id "
            . "WHERE d.id IN (?) AND o.status NOT IN (?) ";

        $args[] = $ids;
        $args[] = [0, 3, 4];

        // By date from to
//        if ($from == null) {
//            $last = $this->getTable()->select('exported_at')->order('exported_at DESC')->limit(1)->fetch();
//
//            if ($last) $from = $last['exported_at'];
//        }
//
//        if ($to == null) {
//            $to = new SqlLiteral('NOW()');
//        }
//
//        $args = [];
//
//        $query = "SELECT d.* FROM " . $this->prefix . "dhl AS d "
//                . "LEFT JOIN " . $this->prefix . "shop_order AS o ON o.id = d.order_id "
//                . "WHERE DATE(o.complete_date) <= ? AND o.status NOT IN (?) ";
//
//        $args[] = $to;
//        $args[] = [0, 3, 4];
//
//        if ($from) {
//            $query .= "AND DATE(o.complete_date) >= ?";
//            $args[] = $from;
//        }

        return $this->db->queryArgs($query, $args)->fetchAssoc('id');
    }


    /**
     * Get packages for tracking
     *
     * @return \Nette\Database\Table\Selection
     */
    public function getTrackingPackages()
    {
        return $this->findBy(['status NOT IN (?)' => [450, 451, 453]], 'exported_at ASC');
    }


    /**
     * Get order packages
     *
     * @param int $orderId
     *
     * @return \Nette\Database\Table\Selection
     */
    public function getOrderPackages($orderId)
    {
        return $this->findBy(['order_id' => $orderId], 'package_count ASC');
    }


    /**
     * Get status list
     *
     * @return array
     */
    public static function getStatusList()
    {
        return [
            150 => _('Prevzatie od zákazníka'),
            151 => _('Prevzatie s výhradou'),
            281 => _('Príjem zo zahraničia'),
            300 => _('Príjem na rozvozové depo'),
            336 => _('Nevyzdvihnutý osobný odber'),
            343 => _('Doručenie na Parcel shop'),
            400 => _('Evidencia na PL'),
            450 => _('Doručené'),
            451 => _('Doručené nekompletné'),
            453 => _('Doručené s výhradou'),
            454 => _('Nedoručené - Nestihol'),
            455 => _('Nedoručené - neskorý príjazd zásielok na depo'),
            456 => _('Nedoručené - porucha vozidla'),
            457 => _('Nedoručené - zlé počasie'),
            458 => _('Nedoručené - príjemca nezastihnutý / oznámenie'),
            459 => _('Nedoručené - príjemca nezastihnutý / bez oznámenia'),
            460 => _('Nedoručené - dohodnutý iný termín'),
            461 => _('Nedoručené - dohodnutý osobný odber'),
            462 => _('Nedoručené - dohodnutá iná adresa'),
            463 => _('Nedoručené - neobjednané'),
            464 => _('Nedoručené - odmietnuté vyexpedované neskoro'),
            465 => _('Nedoručené - odmietnuté zásielka nekompletná'),
            466 => _('Nedoručené - odmietnuté iný dôvod'),
            467 => _('Nedoručené - adresa neúplná/nenájdená'),
            468 => _('Nedoručené - nepripravená hotovosť'),
            469 => _('Nedoručené - dovolenka'),
            470 => _('Nedoručené - zdržanie na colnici'),
            471 => _('Nedoručené - chybné alebo chýbajúce doklady'),
            472 => _('Nedoručené - zásielka poškodená'),
            500 => _('Príjem na centrálu'),
            605 => _('Dobierka - príkaz na zákazníka'),
            606 => _('Dobierka - zaplatené zákazníkovi'),
            613 => _('Dobierka - píkaz partnerovi'),
            614 => _('Dobierka - zaplaten0 partnerovi'),
            710 => _('Preváženie/váženie'),
            718 => _('Späť odosielateľovi'),
            720 => _('Výdaj partnerovi'),
            733 => _('Príjem na výstupnom depe - zahraničie'),
            927 => _('Vymazanie'),
            929 => _('Príjem na iné depo - závlek')
        ];
    }


    /**
     * Get status from status list
     *
     * @param int $code
     *
     * @return string
     */
    public static function getStatus($code)
    {
        if (isset(self::getStatusList()[$code])) {
            return self::getStatusList()[$code];
        }

        return _('Nedefinovaný status');
    }


    /**
     * Change saturday delivery
     *
     * @param array $ids
     */
    public function changeSaturday($ids)
    {
        $list = $this->getPairs(['id IN (?)' => $ids], 'id', 'saturday');

        $update = [];

        foreach ($list as $id => $saturday) {
            $update[$saturday][] = $id;
        }

        if (isset($update[0])) $this->update(['id' => $update[0]], ['saturday' => 1]);
        if (isset($update[1])) $this->update(['id' => $update[1]], ['saturday' => 0]);
    }

}
