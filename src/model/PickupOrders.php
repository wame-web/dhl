<?php

namespace WameCms\Dhl\Model;

use DhlMyApi\PickupOrder;


class PickupOrders
{
    /** @var Dhl */
    private $dhl;

    /** @var Packages */
    private $packages;


    public function __construct(Dhl $dhl, Packages $packages)
    {
        $this->dhl = $dhl;
        $this->packages = $packages;
    }


    /**
     * Create pickup orders to DHL
     *
     * @param array $shopSettings
     * @param \DateTime $form
     * @param \DateTime $to
     *
     * @return null|\stdClass
     *
     * @throws \DhlMyApi\DhlMyApiException
     * @throws \Exception
     */
    public function createPickupOrders($shopSettings, $ids /*$form, $to*/)
    {
        $packages = $this->packages->getPackages($shopSettings, $ids /*$form, $to*/);

        $pickup = new PickupOrder(
            date('YmdHi'),
            '',
            count($packages),
            $this->packages->getSender($shopSettings),
            $shopSettings['tradeEmail'],
            new \DateTime('now'),
            null,
            null,
            ''
        );

        return $this->dhl->getDhlApi()->createPickupOrders([$pickup]);
    }

}
