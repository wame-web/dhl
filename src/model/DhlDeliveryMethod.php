<?php

namespace WameCms\Dhl\Controls;

use Nette\Application\UI\Control;
use Nette\ComponentModel\IContainer;


interface DhlDeliveryMethodFactory
{
    /** @return DhlDeliveryMethod */
    public function create();
}


class DhlDeliveryMethod
{
    /** @var DhlPackageInfoControlFactory */
    private $dhlPackageInfoControlFactory;


    public function __construct(DhlPackageInfoControlFactory $dhlPackageInfoControlFactory)
    {
        $this->dhlPackageInfoControlFactory = $dhlPackageInfoControlFactory;
    }


    public function getControl()
    {
        return $this->dhlPackageInfoControlFactory->create();
    }


    public function getAdminControl()
    {
        return $this->dhlPackageInfoControlFactory->create()->setTemplateName('admin.latte');
    }


    public function callback($presenter, $order, $userInfo, $orderDetail)
    {

    }

}
