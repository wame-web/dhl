<?php

namespace WameCms\Dhl\Model;

use DhlMyApi\PickupOrder;
use WameCms\Dhl\Repositories\DhlRepository;


class Dhl
{
    /** @var array */
    private $options;


    /**
     * Set options from config
     *
     * @param array $options
     *
     * @return $this
     */
    public function setOptions(array $options)
    {
        $this->options = $options;

        return $this;
    }


    /**
     * Get options
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }


    /**
     * Get DhlMyApi
     *
     * @return \DhlMyApi\Dhl
     */
    public function getDhlApi()
    {
        return new \DhlMyApi\Dhl($this->options['username'], $this->options['password'], $this->options['customerId'], self::getTempDir());
    }


    /**
     * Get SOAP
     *
     * @return \SoapClient
     */
    public function getSoap()
    {
        return new \SoapClient('https://myapi.dhlparcel.sk/MyAPI.svc?wsdl', ['trace' => 1, 'exception' => 1]);
    }


    /**
     * Get DHL auth token
     *
     * @return string
     */
    public function getAuthToken(): string
    {
        $path = self::getTempDir() . '/' . md5(implode('|', [$this->options['customerId'], $this->options['username'], $this->options['password']]));

        if (is_file($path) && strtotime('-30 minutes') <= filemtime($path)) {
            return file_get_contents($path);
        }

        $token = $this->login();

        file_put_contents($path, $token);

        return $token;
    }


    /**
     * Login to DHL Api
     *
     * @return string
     *
     * @throws \DhlMyApi\DhlMyApiException
     */
    protected function login(): string
    {
        try {
            $result = $this->getSoap()->Login([
                'Auth' => [
                    'CustId' => $this->options['customerId'],
                    'UserName' => $this->options['username'],
                    'Password' => $this->options['password'],
                ],
            ]);

            return $result->LoginResult->AuthToken;
        } catch (\Exception $e) {
            throw new DhlMyApiException('Login failed', 0, $e);
        }
    }


    /**
     * Get temp dir
     *
     * @return string
     */
    public static function getTempDir()
    {
        $tempDir = ABSOLUTE_PATH . '/../temp/dhl';

        if (!file_exists($tempDir)) mkdir($tempDir, 0755, true);

        return $tempDir;
    }

}
