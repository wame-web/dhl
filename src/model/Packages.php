<?php

namespace WameCms\Dhl\Model;

use App\Model\Shop\ShopOrderRepository;
use App\Model\Shop\ShopOrderUserInfoRepository;
use DhlMyApi\Address;
use DhlMyApi\Dhl;
use DhlMyApi\Package;
use DhlMyApi\Payment;
use DhlMyApi\PdfLabel;
use Nette\Database\Table\ActiveRow;
use WameCms\Dhl\Repositories\DhlRepository;


class Packages
{
    /** @var DhlRepository */
    private $dhlRepository;

    /** @var ShopOrderRepository */
    private $shopOrderRepository;

    /** @var ShopOrderUserInfoRepository */
    private $shopOrderUserInfoRepository;

    /** @var PackageNumber */
    private $packageNumber;

    /** @var array */
    private $options;


    public function __construct(
        DhlRepository $dhlRepository,
        ShopOrderRepository $shopOrderRepository,
        ShopOrderUserInfoRepository $shopOrderUserInfoRepository,
        PackageNumber $packageNumber
    ) {
        $this->dhlRepository = $dhlRepository;
        $this->shopOrderRepository = $shopOrderRepository;
        $this->shopOrderUserInfoRepository = $shopOrderUserInfoRepository;
        $this->packageNumber = $packageNumber;
    }


    /**
     * Set options from config
     *
     * @param array $options
     *
     * @return $this
     */
    public function setOptions(array $options)
    {
        $this->options = $options;

        return $this;
    }


    /**
     * Get packages
     *
     * @param array $shopSettings
     * @param \DateTime $from
     * @param \DateTime $to
     *
     * @return array
     *
     * @throws \Exception
     */
    public function getPackages($shopSettings, $ids /*$from, $to*/)
    {
        $requests = $this->dhlRepository->getOrders($ids /*$form, $to*/);

        if (!$requests) throw new \Exception(_('Nenašli sa žiadne objednávky vyhovujúce filtru.'));

        $orderIds = array_column($requests, 'order_id');

        $ordersList = $this->shopOrderRepository->getObjectPairs(['id' => $orderIds, 'delivery_method_id' => $this->options['deliveryMethod']], 'id');

        if (!$ordersList) throw new \Exception(_('Nenašli sa žiadne objednávky vyhovujúce filtru.'));

        $orderInfoList = $this->shopOrderUserInfoRepository->getObjectPairs(['shop_order_id' => $orderIds], 'shop_order_id');

        $sender = $this->getSender($shopSettings);

        $packages = [];

        foreach ($requests as $requestId => $request) {
            $orderId = $request['order_id'];
            $order = $ordersList[$orderId];
            $orderInfo = $orderInfoList[$orderId];

            $packageNumber = $request['package_number'];
            $packageCount = $request['package_count'];

            $payment = null;

            if ($packageCount == 1 && ($order['payment_method_id'] == $this->options['paymentMethodCOD'] || $order['cod'] > 0)) {
                $payment = new Payment($shopSettings['tradeBankIBAN'], $shopSettings['tradeBankSWIFT'], $order['invoice_number'] ?: $order['order_id'], $this->getOrderPrice($order) * $order->currency_coefficient, in_array($order->currency->code, Payment::CURRENCIES) ? strtoupper($order->currency->code) : Payment::CURRENCY_EUR);
            }

            if ($packageCount == 1) {
                $type = $this->getPackProductType($order, $orderInfo, true);
            } else {
                $type = $this->getPackProductType($order, $orderInfo, false);
            }

            if (!$request['package_number']) {
                $packageNumber = $this->packageNumber->getPackageNumber($type);
                $this->dhlRepository->update(['id' => $requestId], ['package_number' => $packageNumber]);
            }

            $recipient = $this->getRecipient($orderInfo);

            $flags = [];
            if ($request['saturday'] == 1) $flags[] = Package::FLAG_SAT; // saturday delivery

            $packages[$requestId] = (new Package($packageNumber, $type, $this->options['depo'], $recipient, $sender, $payment, $flags, $order['internal_note'] ?: ''))->setPosition($request['package_count'])->setCount($order['count_packages']);
        }

        return $packages;
    }


    /**
     * Get sender address
     *
     * @param array $shopSettings
     *
     * @return Address
     */
    public function getSender($shopSettings)
    {
        return new Address(
            $shopSettings['tradeName'],
            $shopSettings['tradeAddressStreet'],
            $shopSettings['tradeAddressZipCode'],
            $shopSettings['tradeAddressCity'],
            Address::COUNTRY_SK
        );
    }


    /**
     * Get recipient address
     *
     * @param ActiveRow $orderInfo
     *
     * @return Address
     */
    private function getRecipient($orderInfo)
    {
        $country = $this->getCountry($orderInfo);

        if ($orderInfo['setOtherAddress'] == 1) {
            return (new Address(
                ucfirst($orderInfo['shipping_realFirstName']) . ' ' . ucfirst($orderInfo['shipping_realLastName']),
                ucfirst($orderInfo['shipping_street']) . ' ' . ucfirst($orderInfo['shipping_houseNumber']),
                $orderInfo['shipping_zipCode'],
                ucfirst($orderInfo['shipping_city']),
                $this->getCountryCode($country->code)
            ))->setPhone($orderInfo['shipping_phone'] ?: $orderInfo['phone']);
        } else {
            if ($orderInfo['setCompanyInfo'] == 1) {
                return (new Address(
                    ucfirst($orderInfo['companyName']),
                    ucfirst($orderInfo['companyStreet']) . ' ' . ucfirst($orderInfo['companyHouseNumber']),
                    $orderInfo['companyZipCode'],
                    ucfirst($orderInfo['companyCity']),
                    $this->getCountryCode($country->code)
                ))->setPhone($orderInfo['phone']);
            } else {
                return (new Address(
                    ucfirst($orderInfo['realFirstName']) . ' ' . ucfirst($orderInfo['realLastName']),
                    ucfirst($orderInfo['street']) . ' ' . ucfirst($orderInfo['houseNumber']),
                    $orderInfo['zipCode'],
                    ucfirst($orderInfo['city']),
                    $this->getCountryCode($country->code)
                ))->setPhone($orderInfo['phone']);
            }
        }
    }


    /**
     * Calculate order price
     *
     * @param ActiveRow $order
     *
     * @return float
     */
    private function getOrderPrice($order)
    {
        if ($order['cod'] == 0) {
            return (string) number_format($order['order_price_with_tax'] + $order['payment_price'] + $order['delivery_price'], 2, '.', '');
        }

        return (string) $order['cod'];
    }


    /**
     * Get country code
     *
     * @param string $countryCode
     *
     * @return null|string
     */
    private function getCountryCode($countryCode)
    {
        return in_array(strtoupper($countryCode), Address::COUNTRIES) ? strtoupper($countryCode) : null;
    }


    /**
     * Get order country
     *
     * @param ActiveRow $orderInfo
     *
     * @return ActiveRow
     */
    private function getCountry($orderInfo)
    {
        $country = (new \stdClass());
        $country->code = 'sk';

        if ($orderInfo['setOtherAddress'] == 1 && $orderInfo->shipping_country_id) {
            $country = $orderInfo->shipping_country;
        } else {
            if ($orderInfo['setCompanyInfo'] == 1 && $orderInfo->companyCountry_id) {
                $country = $orderInfo->companyCountry;
            } elseif ($orderInfo->_country_id) {
                $country = $orderInfo->country;
            }
        }

        return $country;
    }


    /**
     * Get pack product type
     *
     * @param ActiveRow $order
     * @param ActiveRow $orderInfo
     * @param boolean $cod
     *
     * @return int
     */
    public function getPackProductType($order, $orderInfo, $cod = true)
    {
        $country = $this->getCountry($orderInfo);

        if (!$country) $country = 'sk';

        if ($country->code == 'sk') {
            if (!$cod) return Package::TYPE_SK;

            if ($order['payment_method_id'] == $this->options['paymentMethodCOD'] || $order['cod'] > 0) {
                return Package::TYPE_SK_COD;
            } else {
                return Package::TYPE_SK;
            }
        } else {
            if (!$cod) return Package::TYPE_INTERNATIONAL;

            if ($order['payment_method_id'] == $this->options['paymentMethodCOD'] || $order['cod'] > 0) {
                return Package::TYPE_INTERNATIOANL_COD;
            } else {
                return Package::TYPE_INTERNATIONAL;
            }
        }
    }

}
