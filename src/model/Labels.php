<?php

namespace WameCms\Dhl\Model;

use Nette\Security\User;
use WameCms\Dhl\Model\PdfLabel;
use WameCms\Dhl\Repositories\DhlRepository;


class Labels
{
    /** @var User */
    private $user;

    /** @var Packages */
    private $packages;

    /** @var DhlRepository */
    private $dhlRepository;


    public function __construct(
        User $user,
        Packages $packages,
        DhlRepository $dhlRepository
    ) {
        $this->user = $user;
        $this->packages = $packages;
        $this->dhlRepository = $dhlRepository;
    }


    public function generate($shopSettings, $ids /*$form, $to*/)
    {
        $packages = $this->packages->getPackages($shopSettings, $ids /*$form, $to*/);

        $this->dhlRepository->update(['id IN (?)' => $ids], ['printed_at' => date('Y-m-d H:i:s'), 'printed_user_id' => $this->user->id]);

        header('Content-Type: application/pdf');

        echo PdfLabel::generateLabels($packages, PdfLabel::TYPE_QUARTER);

        exit;
    }

}
