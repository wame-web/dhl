<?php

namespace WameCms\Dhl\Model;

use DhlMyApi\Package;
use Joseki\Application\Responses\PdfResponse;
use Nette\Application\UI\ITemplateFactory;
use Nette\Bridges\ApplicationLatte\ILatteFactory;
use Nette\Security\User;
use WameCms\Dhl\Repositories\DhlRepository;


class TransferList
{
   /** @var ILatteFactory */
    private $latteFactory;

    /** @var Packages */
    private $packages;

    /** @var User */
    private $user;

    /** @var DhlRepository */
    private $dhlRepository;


    public function __construct(
        ILatteFactory $latteFactory,
        Packages $packages,
        User $user,
        DhlRepository $dhlRepository
    ) {
        $this->latteFactory = $latteFactory;
        $this->packages = $packages;
        $this->user = $user;
        $this->dhlRepository = $dhlRepository;
    }


    /**
     * Generate PDF
     *
     * @param array $shopSettings
     * @param \DateTime $form
     * @param \DateTime $to
     *
     * @throws \Exception
     * @throws \Throwable
     */
    public function generate($shopSettings, $ids /*$form, $to*/)
    {
        $packages = $this->packages->getPackages($shopSettings, $ids /*$form, $to*/);

        $this->dhlRepository->update(['id IN (?)' => $ids], ['printed_protocol_at' => date('Y-m-d H:i:s'), 'printed_protocol_user_id' => $this->user->id]);

        $template = $this->latteFactory->create();

        $params = [
            'shopSettings' => $shopSettings,
            'packages' => $packages
        ];

        $html = $template->renderToString(__DIR__ . '/transferList.latte', $params);

        $pdf = new TransferListPdf('L', PDF_UNIT, 'A4', true, 'UTF-8', false);
        $pdf->setPackages($packages);
        $pdf->SetFont('freeserif', '', 10);

        $pdf->AddPage();
        $pdf->writeHTML($html);

        $output = $pdf->Output(NULL, 'S');

        header('Content-Type: application/pdf');

        echo $output;
        exit;
    }

}


class TransferListPdf extends \TCPDF
{
    /** @var Package[] */
    private $packages;

    /** @var int */
    private $countCod = 0;


    /**
     * Set packages
     *
     * @param Package[] $packages
     *
     * @return $this
     */
    public function setPackages($packages)
    {
        $this->packages = $packages;

        return $this;
    }


    /**
     * Get cod price
     *
     * @return string
     */
    private function getCod()
    {
        $return = 0;

        foreach ($this->packages as $package) {
           if (!$package->getPayment()) continue;

           $return += $package->getPayment()->getCodPrice();

           $this->countCod++;
        }

        return $return . ' EUR';
    }


    /** {@inheritdoc} */
    public function Header()
    {
        $this->SetY(5);
        $this->SetFont('freeserif', 'b', 16);

        $this->Cell(20, 0, 'DHL Parcel Slovensko', 0, false, 'L', 0, '', 0, false, 'T', 'M');
    }


    /** {@inheritdoc} */
    public function Footer()
    {
        $cod = $this->getCod();

        $this->SetY(-10);
        $this->SetFont('freeserif', 'b', 10);

        $this->Cell(20, 0, 'Strana ' . $this->getAliasNumPage() . ' z ' . $this->getAliasNbPages(), 0, false, 'L', 0, '', 0, false, 'T', 'M');
        $this->Cell(0, 0, 'Dobierky celkom: ' . $cod, 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(0, 0, 'Celkový počet balíkov: ' . count($this->packages) . '  z toho dobierkových: ' . $this->countCod, 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }

}
