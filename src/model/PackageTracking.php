<?php

namespace WameCms\Dhl\Model;

use Nette\Database\Table\ActiveRow;
use WameCms\Dhl\Repositories\DhlRepository;


class PackageTracking
{
    /** @var Dhl */
    private $dhl;

    /** @var DhlRepository */
    private $dhlRepository;


    public function __construct(Dhl $dhl, DhlRepository $dhlRepository)
    {
        $this->dhl = $dhl;
        $this->dhlRepository = $dhlRepository;
    }


    /**
     * Get package number
     *
     * @param int $packProductType
     *
     * @return int
     */
    public function run($packProductType)
    {
        $packages = $this->dhlRepository->getTrackingPackages();

        foreach ($packages as $package) {
            $packageInfo = $this->getPackageInfo($package['package_number']);

            $data = [
                'status' => $this->getStatus($package, $packageInfo)
            ];

            $this->dhlRepository->update(['id' => $package['id']], $data);
        }
    }


    /**
     * Get new product range from API
     *
     * @param int $packProductType
     *
     * @return int
     */
    private function getPackageInfo($packageNumber)
    {
        $data = [
            'Auth' => [
                'AuthToken' => $this->dhl->getAuthToken(),
            ],
            'Filter' => [
                'PackNumbers' => $packageNumber
            ],
        ];

        $response = $this->dhl->getSoap()->GetPackages($data);

        return $response->GetNumberRangeResult->ResultData->MyApiPackageOut;
    }


    /**
     * Get package status
     *
     * @param ActiveRow $package
     * @param $packageInfo
     *
     * @return int
     */
    private function getStatus($package, $packageInfo)
    {
        if (!isset($packageInfo->PackageStatuses)) return $package['status'];

        return $packageInfo->PackageStatuses->MyApiPackagesOutStatus->StaID;
    }

}
