<?php

namespace WameCms\Dhl\Model;

use Nette\Security\User;
use WameCms\Dhl\Repositories\DhlRepository;


class SendPackages
{
    /** @var User */
    private $user;

    /** @var Dhl */
    private $dhl;

    /** @var Packages */
    private $packages;

    /** @var DhlRepository */
    private $dhlRepository;


    public function __construct(
        User $user,
        Dhl $dhl,
        Packages $packages,
        DhlRepository $dhlRepository
    ) {
        $this->user = $user;
        $this->dhl = $dhl;
        $this->packages = $packages;
        $this->dhlRepository = $dhlRepository;
    }


    /**
     * Send packages to DHL
     *
     * @param array $shopSettings
     * @param \DateTime $form
     * @param \DateTime $to
     *
     * @throws \DhlMyApi\DhlMyApiException
     * @throws \Exception
     */
    public function createPackages($shopSettings, $ids /*$form, $to*/)
    {
        $packages = $this->packages->getPackages($shopSettings, $ids /*$form, $to*/);

        $response = $this->dhl->getDhlApi()->createPackages($packages);

        if ($response instanceof \stdClass) {
            $response = [$response];
        }

        $errors = '';

        foreach ($response as $package) {
            $this->dhlRepository->update(['package_number' => $package->ItemKey], ['exported_at' => date('Y-m-d H:i:s'), 'exported_user_id' => $this->user->id, 'status' => $package->Code]);

            if ($package->Code != 0) {
                $errors .= $package->ItemKey . ' - ' . $package->Message . ' ';
            }
        }

        if ($errors != '') {
            throw new \Exception($errors);
        }
    }

}
