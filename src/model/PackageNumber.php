<?php

namespace WameCms\Dhl\Model;

use WameCms\Dhl\Repositories\DhlProductRepository;


class PackageNumber
{
    /** @var Dhl */
    private $dhl;

    /** @var DhlProductRepository */
    private $dhlProductRepository;


    public function __construct(Dhl $dhl, DhlProductRepository $dhlProductRepository)
    {
        $this->dhl = $dhl;
        $this->dhlProductRepository = $dhlProductRepository;
    }


    /**
     * Get package number
     *
     * @param int $packProductType
     *
     * @return int
     */
    public function getPackageNumber($packProductType)
    {
        $product = $this->dhlProductRepository->findOneBy(['code' => $packProductType]);

        if ($product['next'] >= $product['max']) {
            return $this->getNewRange($packProductType);
        }

        $product = $product->toArray();

        $next = $product['next'];

        $this->dhlProductRepository->update(['code' => $packProductType], ['next' => $next + 1]);

        return $next;
    }


    /**
     * Get new product range from API
     *
     * @param int $packProductType
     *
     * @return int
     */
    private function getNewRange($packProductType)
    {
        $data = [
            'Auth' => [
                'AuthToken' => $this->dhl->getAuthToken(),
            ],
            'NumberRanges' => [
                'NumberRangeRequest' => [
                    'PackProductType' => $packProductType,
                    'Quantity' => $this->dhl->getOptions()['range']
                ]
            ],
        ];

        $response = $this->dhl->getSoap()->GetNumberRange($data);

        $result = $response->GetNumberRangeResult->ResultData->NumberRange;
        $min = $result->From;
        $max = $result->To;

        if (isset($result->ErrorMessage) && $result->ErrorMessage) {
            throw new \Exception($result->ErrorMessage);
        }

        $this->dhlProductRepository->update(['code' => $packProductType], ['min' => $min, 'max' => $max, 'next' => $min + 1]);

        return (int) $min;
    }

}
