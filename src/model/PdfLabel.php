<?php

namespace WameCms\Dhl\Model;

use DhlMyApi\Package;


class PdfLabel extends \DhlMyApi\PdfLabel
{
    /** {@inheritdoc} */
    protected static function generateLabelQuarter(\TCPDF $pdf, Package $package, $position = self::POSITION_TOP_LEFT)
    {
        $pdf->SetAutoPageBreak(true, 0);

        switch ($position) {
            default:
            case self::POSITION_TOP_LEFT:
                $xPositionOffset = 4;
                $yPositionOffset = 4;
                break;

            case self::POSITION_TOP_RIGHT:
                $xPositionOffset = 153;
                $yPositionOffset = 4;
                break;

            case self::POSITION_BOTTOM_LEFT:
                $xPositionOffset = 4;
                $yPositionOffset = 109;
                break;

            case self::POSITION_BOTTOM_RIGHT:
                $xPositionOffset = 153;
                $yPositionOffset = 109;
                break;
        }

        $contact = static::parcelContact();

        // Logo
        if ($contact['logo']) {
            $pdf->Image($contact['logo'], 3 + $xPositionOffset, 3 + $yPositionOffset, 34, '', 'PNG');
        }

        // Contact info
        $pdf->SetFont($pdf->getFontFamily(), '', 9);
        $pdf->Text(3 + $xPositionOffset, 20 + $yPositionOffset, $contact['phone']);
        $pdf->Text(3 + $xPositionOffset, 25 + $yPositionOffset, $contact['email']);
        $pdf->Text(3 + $xPositionOffset, 30 + $yPositionOffset, $contact['web']);

        // Barcode
        $pdf->StartTransform();
        $x = 34 + $xPositionOffset;
        $y = 40 + $yPositionOffset;
        $pdf->Rotate(270, $x, $y);
        $pdf->write1DBarcode($package->getNumber(), 'I25+', $x, $y, 40, 30, 0.3, ['stretch' => true]);

        // Stop Transformation
        $pdf->StopTransform();

        // Barcode number
        $pdf->StartTransform();

        $x = 40 + $xPositionOffset;
        $y = 39 + $yPositionOffset;
        $pdf->Rotate(270, $x, $y);
        $pdf->SetFont($pdf->getFontFamily(), '', 13);
        $pdf->Text($x, $y, $package->getNumber());
        // Stop Transformation
        $pdf->StopTransform();

        // PackagePosition of PackageCount
        $pdf->SetFont($pdf->getFontFamily(), 'B', 13);
        $pdf->MultiCell(20, 0, sprintf('%s/%s', $package->getPosition(), $package->getCount()), ['LTRB' => ['width' => 0.7]], 'C', 0, 0, 116 + $xPositionOffset, 85 + $yPositionOffset, true, 0, false, true, 0);

        // Dobirka
        if ($package->isCashOnDelivery()) {
            $pdf->SetFont($pdf->getFontFamily(), 'B', 13);
            $pdf->SetTextColor(255, 255, 255);
            $pdf->SetFillColor(0, 0, 0);
            $pdf->MultiCell(15, 0, 'DOB.:', ['LTRB' => ['width' => 0.7]], 'L', true, 0, 4 + $xPositionOffset, 85 + $yPositionOffset, true, 0, false, true, 0);
            $pdf->MultiCell(28, 0, sprintf('%s %s', $package->getPayment()->getCodPrice(), $package->getPayment()->getCodCurrency()), ['LTRB' => ['width' => 0.7]], 'R', true, 0, 19 + $xPositionOffset, 85 + $yPositionOffset, true, 0, false, true, 0);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFillColor(255, 255, 255);
        }

        // Prijemce
        $pdf->SetFont($pdf->getFontFamily(), '', 12);
        $pdf->Text(50 + $xPositionOffset, 3 + $yPositionOffset, 'Príjemca:');

        $x = 53 + $xPositionOffset;
        $y = 10 + $yPositionOffset;
        if ($package->getRecipient()->getName()) {
            $pdf->Text($x, $y, $package->getRecipient()->getName());
        }

        $pdf->Text($x, $y + 5, $package->getRecipient()->getContact());
        $pdf->Text($x, $y + 10, $package->getRecipient()->getStreet());
        $pdf->Text($x, $y + 15, sprintf('%s, %s', $package->getRecipient()->getCity(), $package->getRecipient()->getCountry()));

        $pdf->SetFont($pdf->getFontFamily(), 'B', 27);
        $pdf->Text($x, $y + 20, $package->getRecipient()->getZipCode());

        $pdf->SetFont($pdf->getFontFamily(), '', 10);
        $pdf->Text($x, $y + 33, sprintf('Tel.: %s', $package->getRecipient()->getPhone()));

        $pdf->MultiCell(85, 40, '', ['LTRB' => ['width' => 0.7]], 'L', 0, 0, 51 + $xPositionOffset, 9 + $yPositionOffset, true, 0, false, true, 0);

        // Sender
        $pdf->SetFont($pdf->getFontFamily(), '', 12);
        $pdf->Text(50 + $xPositionOffset, 51 + $yPositionOffset, 'Odosielateľ:');

        $x = 53 + $xPositionOffset;
        $y = 58 + $yPositionOffset;
        $pdf->SetFont($pdf->getFontFamily(), '', 10);
        $pdf->Text($x, $y, $package->getSender()->getName());

        $pdf->SetFont($pdf->getFontFamily(), '', 10);
        $pdf->Text($x, $y + 5, $package->getSender()->getName2());

        $pdf->SetFont($pdf->getFontFamily(), '', 10);
        $pdf->Text($x, $y + 10, $package->getSender()->getStreet());

        $pdf->SetFont($pdf->getFontFamily(), '', 10);
        $pdf->Text($x, $y + 15, sprintf('%s %s %s', $package->getSender()->getZipCode(), $package->getSender()->getCity(), $package->getSender()->getCountry()));

        $pdf->SetFont($pdf->getFontFamily(), 'B', 13);
        $pdf->MultiCell(85, 23, '', ['LTRB' => ['width' => 0.7]], 'L', 0, 0, 51 + $xPositionOffset, 57 + $yPositionOffset, true, 0, false, true, 0);

        // Note
        if ($package->getNote()) {
            $x = 53 + $xPositionOffset;
            $y = 84 + $yPositionOffset;

            $pdf->SetXY($x, $y);
            $pdf->SetFont($pdf->getFontFamily(), '', 9);
            $pdf->MultiCell(60, 4, 'Pozn.: ' . $package->getNote(), '', 'L');
        }

        return $pdf;
    }

}
