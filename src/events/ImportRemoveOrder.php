<?php

namespace WameCms\Dhl\Events;

use App\Model\ImportExport\Calls\UpdateOrderData;
use App\Model\ImportExport\Calls\UpdateOrderStatus;
use App\Model\Shop\ShopOrderRepository;
use App\Model\ImportExport\Calls\Call;
use App\Model\ImportExport\ImportExport;
use WameCms\Dhl\Repositories\DhlRepository;


/**
 * Class ImportRemoveOrder
 *
 * Remove packages of deleted orders
 *
 * @package WameCms\Dhl\Events
 */
class ImportRemoveOrder implements Call
{
    /** @var DhlRepository */
    private $dhlRepository;


    public function __construct(DhlRepository $dhlRepository)
    {
        $this->dhlRepository = $dhlRepository;
    }


    public function __invoke(ImportExport $importExport)
    {
        $importIds = array_column($importExport->input, 'ID');

        foreach (array_chunk($importIds, 1000) as $chunk) {
            $query = "DELETE d FROM wame_dhl AS d "
                . "LEFT JOIN wame_shop_order AS o ON o.id = d.order_id "
                . "WHERE o.import_id IN (?)";

            $this->dhlRepository->db->query($query, $chunk);
//            $this->dhlRepository->delete(['order.import_id IN (?)' => $chunk]);
        }
    }

}
