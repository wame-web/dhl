<?php

namespace WameCms\Dhl\Events;

use App\Model\ImportExport\Calls\UpdateOrderData;
use App\Model\ImportExport\Calls\UpdateOrderStatus;
use App\Model\Shop\ShopOrderRepository;
use WameCms\Dhl\Repositories\DhlRepository;


class ImportUpdateOrderStatus
{
    /** @var array */
    private $options;

    /** @var DhlRepository */
    private $dhlRepository;

    /** @var ShopOrderRepository */
    private $shopOrderRepository;


    public function __construct(
//        UpdateOrderData $updateOrderData,
        DhlRepository $dhlRepository,
        ShopOrderRepository $shopOrderRepository
    ) {
        $this->dhlRepository = $dhlRepository;
        $this->shopOrderRepository = $shopOrderRepository;

//        $updateOrderData->onImportUpdateOrderStatus[] = [$this, 'onImportUpdateOrderStatus'];
    }


    /**
     * Set options from config
     *
     * @param array $options
     *
     * @return $this
     */
    public function setOptions(array $options)
    {
        $this->options = $options;

        return $this;
    }


    public function onUpdateOrderData($importIds, $importType)
    {
        if (count($importIds) == 0) return;

        $orders = $this->shopOrderRepository->getObjectPairs(['import_id IN (?)' => $importIds, 'import_type' => $importType, 'delivery_method_id' => $this->options['deliveryMethod']], 'id');

//        $find = $this->dhlRepository->getPairs(['order_id IN (?)' => array_keys($orders)], 'order_id', 'order_id');
//
//        foreach ($orders as $order) {
//            if ($order['id'] == null) continue;
//            if (isset($find[$order['id']])) continue;
//
//            for ($i = 1; $i <= $order['count_packages']; $i++) {
//                $this->dhlRepository->insert(['order_id' => $order['id'], 'package_count' => $i]);
//            }
//        }

        $find = $this->findPackages(array_keys($orders));

        foreach ($orders as $order) {
            if ($order['id'] == null) continue;
//                if (isset($find[$order['id']])) continue;

            for ($i = 1; $i <= $order['count_packages']; $i++) {
                if (isset($find[$order['id']]) && isset($find[$order['id']][$i])) continue;

                $this->dhlRepository->insertOrUpdate(['order_id' => $order['id'], 'package_count' => $i]);
            }

            $this->dhlRepository->delete(['order_id' => $order['id'], 'package_count > ?' => $i]);
        }
    }


    public function onImportUpdateOrderStatus($importIds, $importType, $status)
    {
        if ($status == $this->options['orderStatus']) {

            $orders = $this->shopOrderRepository->getObjectPairs(['import_id IN (?)' => $importIds, 'import_type' => $importType, 'delivery_method_id' => $this->options['deliveryMethod'], 'status' => $this->options['orderStatus']], 'id');

//            $find = $this->dhlRepository->getPairs(['order_id IN (?)' => array_keys($orders)], 'order_id', 'order_id');
            $find = $this->findPackages(array_keys($orders));

            foreach ($orders as $order) {
                if ($order['id'] == null) continue;
//                if (isset($find[$order['id']])) continue;

                for ($i = 1; $i <= $order['count_packages']; $i++) {
                    if (isset($find[$order['id']]) && isset($find[$order['id']][$i])) continue;

                    $this->dhlRepository->insertOrUpdate(['order_id' => $order['id'], 'package_count' => $i]);
                }

                $this->dhlRepository->delete(['order_id' => $order['id'], 'package_count > ?' => $i]);
            }
        }
    }


    private function findPackages($orders)
    {
        $list = $this->dhlRepository->findBy(['order_id IN (?)' => array_keys($orders)]);

        $return = [];

        foreach ($list as $item) {
            $return[$item['order_id']][$item['package_count']] = $item['package_number'];
        }

        return $return;
    }

}
