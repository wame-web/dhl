# DHL myAPI for CMS WAME admin 3

Slúži na preposlanie údajov medzi Eshopom a DHL myAPI

**Postupnosť na kubicasport:**
- Objednávka musí mať spôsob dopravy DHL
- V Money zaškrtnú checkbox Expedovať
- Import prenesie zmenu do CMS a v tabuľke `wame_dhl` vytvorí balíky podľa Používateľského stĺpca Počet balíkov v Money
- Potom sa objednávky zobrazia v gride na adrese `/admin/sk/dhl/`


## Inštalácia

- Do `composer.json` pridať
```
"repositories": [
    { "type": "composer", "url": "https://packages.wame.sk/" }
],
"require": {
    "wamecms/dhl": "dev-master"
}
```

- Z priečinka `vendor/wamecms/dhl/db/migrations` skopírovať migrácie do `db/migrations` a spustiť ich


## Implementácia

- Do `./app/config/config.neon`
```
extensions:
    WameCms.dhl: WameCms\Dhl\DI\DhlExtension
    
WameCms.dhl:
    customerId: ''
    username: ''
    password: ''
    depo: 52
    deliveryMethod: 2
    paymentMethodCOD: 3
    orderStatus: 11
    pickupButton: true
```

| name | popis |
| ---- | ----- |
| `customerId` | DHL customerId |
| `username` | DHL username |
| `password` | DHL password |
| `depo` | ID DHL depa odkiaľ budú preberané balíky |
| `deliveryMethod` | ID spôsobu dopravy z CMS |
| `paymentMethodCOD` | ID spôsobu platby pre dobierku z CMS |
| `orderStatus` | Stav objednávky z CMS kedy sa objednávka dostane do tabuľky wame_dhl |
| `pickupButton` | Zobrazí/Skryje tlačidlo na zavolanie kuriéra |


## Komponenty

### DhlExport

Komponenta s rýchlym prehľadom balíkov pre DHL

- Na mieste kde ju chceme použiť zvyčajne na Dashboard v administrácii pridáme `trait` do presentra

`app/adminModule/presenters/DashboardPresenter.php`
```
use WameCms\Dhl\Controls\DhlExportControlTrait;
```

- Do šablóny pridáme 
```
{control dhlExport}
```

### DhlPackageInfo

Komponenta na zobrazenie informácii o balíku v objednávke v administrácii a frontende

